<?php
namespace CronExpression\Command;

use CronExpression\Field\CronCommandField;
use CronExpression\Field\DayOfMonthField;
use CronExpression\Field\DayOfWeekField;
use CronExpression\Field\HourField;
use CronExpression\Field\MinuteField;
use CronExpression\Field\MonthField;
use CronExpression\Service\CronParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseExpression extends Command
{
    protected function configure()
    {
        $this->setName('cron:expression:parse')
             ->setDescription('Parses cron expression.')
             ->addArgument('cron', InputArgument::REQUIRED, 'The cron expression')
             ->setHelp('This command allows you to parse a cron string and expands each field to show the 
                        times it will run');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cronExpression = $input->getArgument('cron');

        $minuteField = new MinuteField();
        $hourField = new HourField();
        $dayOfMonthField = new DayOfMonthField();
        $monthField = new MonthField();
        $dayOfWeekField = new DayOfWeekField();
        $commandField = new CronCommandField();

        $cronParserService = new CronParser(
            $minuteField,
            $hourField,
            $dayOfMonthField,
            $monthField,
            $dayOfWeekField,
            $commandField
        );

        $results = $cronParserService->parse($cronExpression);

        $resultsTable = new Table($output);
        $resultsTable->setStyle('borderless');
        $resultsTable->setHeaders([]);

        $rows = [];
        foreach ($results as $key => $value) {
            $rows[] = [$key, implode(' ', $value)];
        }

        $resultsTable->setRows($rows);
        $resultsTable->render();
    }

}