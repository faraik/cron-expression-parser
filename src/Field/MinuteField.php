<?php
namespace CronExpression\Field;

class MinuteField
{
    public function describeMinute(string $minuteExpression) : array
    {
        // TODO refactor all logic with description comments below to separate functions and implement switch case for character check
        // TODO validate supplied values fall within 60min range otherwise throw exception
        $result = [];
        $minuteExpression =  preg_replace('/\s+/', '', $minuteExpression);

        // at minute
        if (is_numeric($minuteExpression)) {
            $result[] = $minuteExpression;
            return $result;
        }

        // at minute increments
        if (($pos = strpos($minuteExpression, '/')) !== false) {
            $startMinute = substr($minuteExpression, $pos - 1);
            $minuteIncrement = substr($minuteExpression, $pos + 1);

            if ($startMinute == '*') {
                foreach (range(0, 59, (int)$minuteIncrement) as $minute) {
                    $result[] = $minute;
                }
                return $result;
            }

            foreach (range((int)$startMinute, 59, (int)$minuteIncrement) as $minute) {
                $result[] = $minute;
            }
            sort($result, SORT_NUMERIC);
            return $result;
        }

        // at minute range
        if (($pos = strpos($minuteExpression, '-')) !== false) {
            $minuteStart = substr($minuteExpression, $pos + 1);
            $minuteEnd = substr($minuteExpression, $pos - 1);
            foreach (range((int)$minuteStart, (int)$minuteEnd) as $minute) {
                $result[] = $minute;
            }
            sort($result, SORT_NUMERIC);
            return $result;
        }

        // at minute values
        if (strpos($minuteExpression, ',') !== false) {
            $result = explode(',', $minuteExpression);
            sort($result, SORT_NUMERIC);
            return $result;
        }

    }

}