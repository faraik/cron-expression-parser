<?php
namespace CronExpression\Service;

use CronExpression\Field\CronCommandField;
use CronExpression\Field\DayOfMonthField;
use CronExpression\Field\DayOfWeekField;
use CronExpression\Field\HourField;
use CronExpression\Field\MinuteField;
use CronExpression\Field\MonthField;

class CronParser
{
    private $minuteField;
    private $hourField;
    private $dayOfMonthField;
    private $monthField;
    private $dayOfWeekField;
    private $cronCommandField;

    public function __construct(
        MinuteField $minuteField,
        HourField $hourField,
        DayOfMonthField $dayOfMonthField,
        MonthField $monthField,
        DayOfWeekField $dayOfWeekField,
        CronCommandField $cronCommandField
    )
    {
        $this->minuteField = $minuteField;
        $this->hourField = $hourField;
        $this->dayOfMonthField = $dayOfMonthField;
        $this->monthField = $monthField;
        $this->dayOfWeekField = $dayOfWeekField;
        $this->cronCommandField = $cronCommandField;
    }

    public function parse(string $cronExpression) : array
    {
        $result = [];
        //TODO:: validate expression
        list($minute, $hour, $dayOfMonth, $month, $dayOfWeek, $command) = explode(' ', $cronExpression);

        $result['minute'] = $this->minuteField->describeMinute($minute);
        $result['hour'] = ['TODO'];     // TODO:: $this->hourField->describeHour($hour)
        $result['day of month'] = ['TODO']; // TODO:: $this->dayOfMonthField->describeDay($dayOfMonth)
        $result['month'] = ['TODO']; // TODO:: $this->monthField->describeMonth($month)
        $result['day of week'] = ['TODO']; // TODO:: $this->dayOfWeekField->describeDay($dayOfWeek)
        $result['command'] = [$command];
        return $result;
    }
}