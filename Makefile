docker_sh   = docker exec -w /app -it shell
docker_sh_c = $(docker_sh) /bin/bash -c

build:
	@docker-compose down
	@docker-compose build --no-cache
	@docker-compose up -d

install:
	@$(docker_sh_c) "composer install"

shell:
	@$(docker_sh_c) "export COLUMNS=`tput cols`; export LINES=`tput lines`; exec bash"

run_app:
	@$(docker_sh_c) "php bin/app.php cron:expression:parse '$(cron)'"

up: build install

test:
	@$(docker_sh_c) "vendor/bin/phpunit -c phpunit.xml"

.PHONY: build install shell up test
