Instructions
=============

## How start docker container:

`make up`

## How to run tests (after you've implemented them - please make sure you app runs all tests you have via this command)

`make test`

## Enter container

`make shell`

## How to run the app

`make run_app cron='{your_cron_expression}'` 

e.g `make run_app cron='*/15 0 1,15 * 1-5 /usr/bin/find'`  

## TODO

Commented with TODO in various places in code to give further insight to approach I was going for and various other things I would 
have liked to do if time permitted