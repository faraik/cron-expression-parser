<?php

namespace CronExpression\Tests\Command;

use CronExpression\Command\ParseExpression;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class ParseExpressionTest extends TestCase
{

    public function testParseExpressionCommand()
    {
        $application = new Application();
        $application->add(new ParseExpression());

        $command = $application->find('cron:expression:parse');

        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array_merge(array('command' => $command->getName()), ['cron' => '*/15 0 1,15 * 1-5 /usr/bin/find'])
        );

        $this->assertNotEmpty($commandTester->getDisplay());
    }
}