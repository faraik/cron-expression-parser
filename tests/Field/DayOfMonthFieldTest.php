<?php
namespace CronExpression\Tests\Field;

use CronExpression\Field\DayOfMonthField;
use PHPUnit\Framework\TestCase;

class DayOfMonthFieldTest extends TestCase
{
    public function testCreateDayOfMonth()
    {
        $dayOfMonthField = new DayofMonthField();
        $this->assertInstanceOf(DayOfMonthField::class, $dayOfMonthField);
    }

}