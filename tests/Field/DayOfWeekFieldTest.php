<?php
namespace CronExpression\Tests\Field;

use CronExpression\Field\DayOfWeekField;
use PHPUnit\Framework\TestCase;

class DayOfWeekFieldTest extends TestCase
{
    public function testCreateDayOfWeek()
    {
        $dayOfWeekField = new DayofWeekField();
        $this->assertInstanceOf(DayOfWeekField::class, $dayOfWeekField);
    }

}