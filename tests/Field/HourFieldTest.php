<?php
namespace CronExpression\Tests\Field;

use CronExpression\Field\HourField;
use PHPUnit\Framework\TestCase;

class HourFieldTest extends TestCase
{
    public function testCreateHour()
    {
        $hourField = new HourField();
        $this->assertInstanceOf(HourField::class, $hourField);
    }

}