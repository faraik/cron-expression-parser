<?php
namespace CronExpression\Tests\Field;

use CronExpression\Field\MinuteField;
use PHPUnit\Framework\TestCase;

class MinuteFieldTest extends TestCase
{
    private $minuteField;

    public function setUp()
    {
        $this->minuteField = new MinuteField();
    }

    public function testCreateMinute()
    {
        $this->assertInstanceOf(MinuteField::class, $this->minuteField);
    }

    /**
     * @dataProvider minuteExpressionProvider
     */
    public function testDescribeMinute($expression, $expected)
    {
        $minuteExpression = $expression;

        $result = $this->minuteField->describeMinute($minuteExpression);

        $this->assertEquals($expected, $result);
    }

    public function minuteExpressionProvider()
    {
        return [
            ['*/15', [0, 15, 30, 45]],
            ['1-5', [1, 2, 3, 4, 5]],
            ['10,20', [10, 20]]
        ];
    }





}