<?php
namespace CronExpression\Tests\Field;

use CronExpression\Field\CronCommandField;
use PHPUnit\Framework\TestCase;

class CronCommandFieldTest extends TestCase
{
    public function testCreateCronCommand()
    {
        $cronCommandField = new CronCommandField();
        $this->assertInstanceOf(CronCommandField::class, $cronCommandField);
    }

}