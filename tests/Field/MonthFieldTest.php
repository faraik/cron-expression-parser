<?php
namespace CronExpression\Tests\Field;

use CronExpression\Field\MonthField;
use PHPUnit\Framework\TestCase;

class MonthFieldTest extends TestCase
{
    public function testCreateMonth()
    {
        $monthField = new MonthField();
        $this->assertInstanceOf(MonthField::class, $monthField);
    }

}