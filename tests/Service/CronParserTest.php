<?php

namespace CronExpression\Tests\Service;

use CronExpression\Field\CronCommandField;
use CronExpression\Field\DayOfMonthField;
use CronExpression\Field\DayOfWeekField;
use CronExpression\Field\HourField;
use CronExpression\Field\MinuteField;
use CronExpression\Field\MonthField;
use CronExpression\Service\CronParser;
use PHPUnit\Framework\TestCase;


class CronParserTest extends TestCase
{
    public function testParse()
    {
        $cronParser = new CronParser(
            new MinuteField(),
            new HourField(),
            new DayOfMonthField(),
            new MonthField(),
            new DayOfWeekField(),
            new CronCommandField()
        );

        $result = $cronParser->parse('*/15 0 1,15 * 1-5 /usr/bin/find');

        $expectedResult = [
            'minute' => [0,15,30,45],
            'hour' => 1,
            'day of month' => [1,15],
            'month' => [1,2,3,4,5,6,7,8,9,10,11,12],
            'day of week' => [1,2,3,4,5],
            'command' => '/usr/bin/find'
        ];

        //$this->assertEquals($expectedResult, $result);
        $this->markTestIncomplete();
    }


}